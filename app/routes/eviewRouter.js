const express = require("express");

const router = express.Router();

router.get("/reviews",(req,res) => {
    res.status(200).json({
        message: `Get All review`
    })
})

router.get("/reviews/:reviewId", (req,res) => {
    let reviewId = req.params.reviewId;
    res.status(200).json({
        message: `Get reviewId = ` + reviewId
    })
})

router.post("/reviews",(req,res) => {
    res.status(200).json({
        message: `Create review`
    })
})

router.put("/reviews/:reviewId", (req,res) => {
    let reviewId = req.params.reviewId;
    res.status(200).json({
        message: `Update reviewId = ` + reviewId
    })
})

router.delete("/reviews/:reviewId", (req,res) => {
    let reviewId = req.params.reviewId;
    res.status(200).json({
        message: `Delete reviewId = ` + reviewId
    })
})

module.exports = router;
