const express = require('express');
//import thu vien mongoose
const mongoose = require("mongoose")
const app = new express();
const path = require("path");
const { execPath } = require('process');

const port = 8000;

//import router modules
const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/eviewRouter")

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}))
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(express.static(path.join(__dirname,'/views')))

//middleware chạy ra thời gian &  phương thức req
app.use((req,res,next)=> {
    console.log(new Date());
    next()
}, (req,res,next)=> {
    console.log(req.method);
    next()
})

// app.use((req,res,next)=> {
//     console.log(req.method);
//     next()
// })

mongoose.connect("mongodb://localhost:27017/CRUD_Course365",(error)=>{
if(error) throw error;
console.log("connect successfully")
})

app.get("/",(req,res)=> {
    res.sendFile(path.join(__dirname,"/views"))
})


app.use(courseRouter)
app.use(reviewRouter)



app.listen(port,() => {
    console.log(`app listen on port ${port}`)
})
