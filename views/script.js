var gCourePopular = [];
var gtrendingCourse = []
$(document).ready(function () {
    $.ajax({
        async: false,
        url: "http://localhost:8000/courses",
        type: "GET",
        success: (data) => {
            gCourePopular = data.Courses
            console.log(data)
        },
        error: (error) => {
            console.log(error);
        }
    })
    
    onPageLoadingFindPopular();
    onPageLoadingFindTrending();
})

function onPageLoadingFindPopular() {
    popularCourse = gCourePopular.filter(item => item.isPopular == true);
    console.log("Popular Coures:");
    console.log(popularCourse);
    loadDataToPopular(popularCourse);
}
function onPageLoadingFindTrending() {
    gtrendingCourse = gCourePopular.filter(item => item.isTrending == true);
    console.log("Trending Coures:");
    console.log(gtrendingCourse);
    loadDataToTrending(gtrendingCourse);
}

function loadDataToPopular(dataPopular) {
    var html = dataPopular.map(function(element) {
        return ` <div class="card">
        <img src = '${element.coverImage}' class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title text-primary" style="font-size: 17px;"> ${element.courseName}
          </h5>
          <p class="card-text text-muted"> <i class="far fa-clock"></i> &ensp; ${element.duration} &nbsp; ${element.level}</p>
          <div class="text-center">
            <span style="font-weight: 700;">$${element.discountPrice}</span> &ensp;
            <span style="text-decoration: line-through; " class="text-muted">$ ${element.price}</span>
          </div>
        </div>
        <div class="card-footer">
          <img class="img-format" src="${element.teacherPhoto}">
          <small class="text-muted">${element.teacherName}</small> &emsp; &emsp; &emsp;
          <i class="far fa-bookmark"></i>
        </div>
      </div>`
    })
    $("#loadDataToPopular").html(html.join(''))
}

function loadDataToTrending(dataTrending) {
    var html = dataTrending.map(function(element) {
        return `  <div class="card">
        <img src="${element.coverImage}" class="card-img-top" alt="...">
        <div class="card-body" style="height: 170px;">
          <h5 class="card-title text-primary" style="font-size: 17px;">${element.courseName}
          </h5>
          <p class="card-text text-muted"> <i class="far fa-clock"></i> &ensp; ${element.duration} &nbsp; ${element.level}</p>
          <div class="text-center">
            <span style="font-weight: 700;">$${element.discountPrice}</span> &ensp;
            <span style="text-decoration: line-through; " class="text-muted">$${element.price}</span>
          </div>
        </div>
        <div class="card-footer">
          <img class="img-format" src="${element.teacherPhoto}">
          <small class="text-muted">${element.teacherName}</small> &emsp; &emsp; 
          <i class="far fa-bookmark"></i>
        </div>
      </div>
      </div>`
    })
    $("#loadDataToTrending").html(html.join(''))
}
