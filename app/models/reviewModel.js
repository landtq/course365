// import thu vien mongoose
const mongoose = require("mongoose");

//khai bao class Schema cua mongoose
const Schema = mongoose.Schema;

//khai bao review Schema
const reviewSchema = new Schema({
    //_id co the khai bao hoac ko
    // _id: {
    //     type: mongoose.Types.ObjectId,
    //     require: true
    // },
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        require: false
    },
}, {
    timestamps: true
}
)

module.exports = mongoose.model("review", reviewSchema)