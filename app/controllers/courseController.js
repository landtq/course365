// import thu vien moongoose 
const mongoose = require("mongoose");

//import courseModel vào file 
const courseModel = require("../models/courseModel")

//create course
const createCourse = (req, res) => {
    //B1: thu thập dữ liệu
    let body = req.body;
    //B2: validate dữ liệu
    if (!body.courseCode) {
        return res.status(400).json({
            message: `courseCode is required`
        })
    }
    if (!body.courseName) {
        return res.status(400).json({
            message: `courseName is required`
        })
    }
    if (!body.discountPrice) {
        return res.status(400).json({
            message: `discountPrice is required`
        })
    }
    if (!body.duration) {
        return res.status(400).json({
            message: `duration is required`
        })
    }
    if (!body.level) {
        return res.status(400).json({
            message: `level is required`
        })
    }
    if (!body.coverImage) {
        return res.status(400).json({
            message: `coverImage is required`
        })
    }
    if (!body.teacherName) {
        return res.status(400).json({
            message: `teacherName is required`
        })
    }
    if (!body.teacherPhoto) {
        return res.status(400).json({
            message: `teacherPhoto is required`
        })
    }
    if (!Number.parseInt(body.price) || body.price < 0 || body.price < body.discountPrice) {
        return res.status(400).json({
            message: ` price is invalied`
        })
    }
    if (!Number.parseInt(body.discountPrice) || body.discountPrice < 0 ) {
        return res.status(400).json({
            message: ` discountPrice is invalied`
        })
    }
    let newCourseData = {
    //B3: Gọi model thực hiện các thao tác nghiệp vụ 
        _id: mongoose.Types.ObjectId(),
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        // isPopular: body.isPopular,
        // isTrending: body.isTrending
    }
    courseModel.create(newCourseData, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(201).json({
            message: `Create sucessful`,
            newCourse: data
        })
    })
}
//get all course
const getAllCourse = (req, res) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ 
    courseModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: ` all course was geted sucessfully`,
            Courses: data
        })
    })
}

//get course by Id
const getCourseById = (req, res) => {
    //B1: thu thập dữ liệu
    let courseId = req.params.courseId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            message: `courseId is invalied`
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ 
    courseModel.findById(courseId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(201).json({
            message: ` get course by Id ${courseId} sucessfully`,
            course: data
        })
    })
}

// update course by id
const updateCourseById = (req, res) => {
    //B1: thu thập dữ liệu
    let courseId = req.params.courseId;
    let body = req.body;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            message: `courseId is invalied`
        })
    }
    //Bóc tách trường hợp undefined
    if (body.courseCode !== undefined && body.courseCode == "") {
        return res.status(400).json({
            message: `courseCode is required`
        })
    }
    if (body.duration !== undefined && body.duration == "") {
        return res.status(400).json({
            message: `duration is required`
        })
    }
    if (body.level !== undefined && body.level == "") {
        return res.status(400).json({
            message: `level is required`
        })
    }
    if (body.coverImage !== undefined && body.coverImage == "") {
        return res.status(400).json({
            message: `coverImage is required`
        })
    }
    if (body.teacherName !== undefined && body.teacherName == "") {
        return res.status(400).json({
            message: `teacherName is required`
        })
    }
    if (body.teacherPhoto !== undefined && body.teacherPhoto == "") {
        return res.status(400).json({
            message: `teacherPhoto is required`
        })
    }
    if (body.price !== undefined && (!Number.parseInt(body.price) || body.price < 0) || body.price <body.discountPrice) {
        return res.status(400).json({
            message: "Price is invalid!"
        })
    }
    if (body.discountPrice !== undefined && (!Number.parseInt(body.discountPrice) || body.discountPrice < 0) ) {
        return res.status(400).json({
            message: "discountPrice is invalid!"
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ 
    let courseUpdate = {
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
    }
    courseModel.findByIdAndUpdate(courseId,courseUpdate,(error,data)=>{
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `Update successful`,
            updateCourse: data
        })
    })
}


// delete course by Id
const deleteCourseById = (req, res) => {
    //B1: thu thập dữ liệu
    let courseId = req.params.courseId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            message: "Course ID is invalid!"
        })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ 
    courseModel.findByIdAndDelete(courseId,(error, data) => {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(204).json({
            message: "Delete course successfully"
        })
    })
}

module.exports = {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}