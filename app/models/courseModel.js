// khai bao mongoose
const mongoose = require("mongoose");

//khai bao class schema cua thu vien mongooseJs
const Schema = mongoose.Schema;

//khai bao course schema
const courseSchema = new Schema({
    courseCode: {type: String, unique: true, required: true},
    courseName: {type: String, required: true},
    price: {type: Number, required: true},
    discountPrice: {type: Number, required: true},
    duration: {type: String,  required: true},
    level: {type: String, required: true},
    coverImage: {type: String, required: true},
    teacherName: {type: String, required: true},
    teacherPhoto: {type: String, required: true},
    isPopular: {type: Boolean, default: true},
    isTrending: {type: Boolean, default: false}
}, {
    timestamps: true
})

module.exports = mongoose.model("courses", courseSchema)